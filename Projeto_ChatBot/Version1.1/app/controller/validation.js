
function validar_login(){

	let email = form_login.email.value;
	let password = form_login.password.value;

	if (email == "" || email.indexOf('@') == -1) {

		alert ('Por favor, insira seu email corretamente!');
		form_login.email.focus();
		return false;
	}
	
	else if (password == "" || password.length<=5) {

		alert ('Por favor, insira sua senha de 6 ou mais dígitos!');
		form_login.password.focus();
		return false;
	}
}