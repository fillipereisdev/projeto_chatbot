<?php

$username = 'root';
$password = '';

try {
    
    $conn = new PDO('mysql:host=localhost;dbname=sir_stake', $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $error) {

    echo "<script> alert('Erro ao conectar!'); </script>";
    echo 'ERROR: ' . $error->getMessage();
}

?>