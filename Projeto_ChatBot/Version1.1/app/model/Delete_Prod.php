<?php

require_once ('Connection.php');

        $id_prod = $_POST['id_prod'];

 try {
        $stmt = $conn->prepare('DELETE FROM prod_ins WHERE id_prod = ?');
        $stmt->bindParam(1, $id_prod);
        $stmt->execute();

        echo "<script>
        alert('Produto excluído com sucesso!')
        window.location.replace('http://localhost/Projeto_ChatBot/Version1.1/modulo_admin/views/create_cadastro.php');
    </script>";
     
 } catch (PDOException $error) {
         echo "Erro ao excluir: " . $error->getMessage();
 }

?>