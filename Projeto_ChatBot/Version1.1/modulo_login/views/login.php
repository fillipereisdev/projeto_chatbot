<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title> Módulo de Login </title>
	
	<!-- Link referenciados de css e bootstrap -->

  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../fonts/icomoon/style.css">
  <link rel="stylesheet" href="../css/owl.carousel.min.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">

</head>
<body>

	<!-- Divs da tela de login responsiva -->

	<div class="half">
   
    <div class="contents order-2 order-md-1">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-5">
            <div class="form-group">
              <div class="text-center">
                <img src="http://localhost/Projeto_ChatBot/Version1.1/modulo_admin/images/users/logo.jpeg">
                </div>

                <!-- Formulário via post e validando campos em html -->
                <form name="form_login" action="validation.js" method="post">
                  <div class="form-group first">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" placeholder="Por gentileza, informe um e-mail válido..." required="email" id="email"> 
                  </div>

                  <div class="form-group last mb-3">
                    <label for="password">Senha:</label>
                    <input type="password" class="form-control" placeholder="Por gentileza, informe sua senha..." required="password" id="password">
                  </div>

                  <div class="d-sm-flex mb-5 align-items-center">
                    <label class="control control--checkbox mb-3 mb-sm-0"><span class="caption">Lembrar-me</span>
                      <input type="checkbox" checked="checked"/>
                      <div class="control__indicator"></div>
                    </label>
                    <span class="ml-auto"><a href="create_user.html" class="forgot-pass">Não possui cadastro? Clique aqui</a></span> 
                  </div>

                  <input type="submit" onclick="return validar_login();" value="Entrar" class="btn btn-block btn-success">

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    
    <!-- Links referenciados javascript -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <script src="http://localhost/Projeto_ChatBot/Version1.1/app/controller/validation.js"> </script>
    
  </body>
  </html>