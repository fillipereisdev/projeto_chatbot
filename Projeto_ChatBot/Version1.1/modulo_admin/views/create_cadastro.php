
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">

    <title> Todos os produtos </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Bootstrap CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- CSS -->
    <link href="css/style.css" rel="stylesheet">

    <?php require_once('../includes/header.php'); ?>

</head>

<body>  <!-- Cabeçalho -->

    <!-- Conteúdo da listagem de clientes -->
    <div id="page-wrapper">
        <div class="container-fluid"> 
            <div class="row bg-title">
                <div class="col-lg-12"> 
                    <h4 class="page-title">Todos os produtos / insumos</h4>
                    <ol class="breadcrumb">
                        <li><a href="cad_option.php">Cadastros</a></li>
                        <li class="active">Produtos / Insumos</li>
                    </ol>
                </div> 
                <!-- /.col-lg-12 -->
            </div>
            
            <form name="Cadastrar" method="POST" action="http://localhost/Projeto_ChatBot/Version1.1/app/model/Create_Prod.php">
                <div class="form-group">
                    <label class="col-md-12">Nome do produto:</label>
                    <div class="col-md-7">
                        <input type="text" name="nome_prod" required="text"
                        class="form-control"> </div>
                    </div>                               

                    <div class="form-group">
                        <div class="col-md-7">
                            <label for="categ_prod">Categoria:</label>
                            <select class="form-control form-control-lg" name="categ_prod">
                              <option>Selecione a opção</option>
                              <option>Pão</option>
                              <option>Carne / Bife</option>
                              <option>Doces / Confeitaria</option>
                              <option>Bebida</option>
                              <option>Molho</option>
                              <option>Conservas</option>
                              <option>Legume / Verdura</option>
                              <option>Congelados</option>
                              <option>Embalagem</option>
                          </select> </div> </div>

                          <div class="form-group">
                            <div class="col-md-7">
                                <label for="mod_compra">Modalidade de compra:</label>
                                <select class="form-control form-control-lg" name="mod_compra">
                                  <option>Selecione a opção</option>
                                  <option>Unidade</option>
                                  <option>Caixa / Fardo</option>
                              </select> </div> </div>

                              <div class="form-group">
                                <label class="col-md-12">Quantidade:</label>
                                <div class="col-md-3">
                                    <input type="text" name="qtd" required="text"
                                    class="form-control"> </div>
                                </div> 

                                <div class="form-group">
                                    <div class="col-sm-12"><br>
                                        <button type="submit" class="btn btn-success">Cadastrar</button>
                                    </div>

                                </div>

                            </div>

                        </form><br>

                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="white-box">
                                    <div class="table-responsive">
                                        <table class="table">

                                            <tr>
                                                <th>Nome do produto</th>
                                                <th>Categoria</th>
                                                <th>Modalidade de compra</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                                <th>Ações</th>
                                                <th></th> <!-- Faz o crud alterar e excluir -->
                                            </tr>

                                    <?php // Read de todos os produtos
                                        require_once('C:/xampp/htdocs/Projeto_ChatBot/Version1.1/app/model/Connection.php');
                                    try {

                                        $stmt = $conn->prepare("SELECT * FROM prod_ins");
                                        if ($stmt->execute()) {
                                            while ($res = $stmt->fetch(PDO::FETCH_OBJ)) {
                                                echo "<tr>";
                                                echo "<td>"
                                                .$res->nome_prod."</td><td>"
                                                .$res->categ_prod."</td><td>"
                                                .$res->mod_compra."</td><td>"
                                                .$res->qtd."</td><td>"

                                                ."<td> 

                                                <a href='http://localhost/Projeto_ChatBot/Version1.1/app/model/Delete_Prod.php?id_prod=<?php echo $id_prod; ?>'
                                                <td class='btn btn-danger'>Excluir</td>
                                                </a>
                                                                                                                       
                                                </td>";

                                            }
                                        } else {
                                            echo "Erro: Não foi possível recuperar os dados do banco de dados";
                                        }
                                    } catch (PDOException $erro) {
                                        echo "Erro: ".$erro->getMessage();
                                    }
                                    ?>

                                </table>

                            </div>

                        </div>

                    </div>
                    
                </div>
                
            </div>
            <?php require_once('../includes/footer.php'); ?>
            <br><br>
        </div>

    </div>

</div>

<!-- /.container-fluid -->

</div>

<!-- /#page-wrapper -->

</div>

<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Tema padrão do admin -->
<script src="js/myadmin.js"></script>
<script src="js/dashboard1.js"></script>
<script src="Projeto_ChatBot/Version1.1/app/controller/logoff.js"> </script>

</body>
</html>