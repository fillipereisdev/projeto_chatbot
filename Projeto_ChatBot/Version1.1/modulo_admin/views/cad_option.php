<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">
    
    <title>Todos os cadastros</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Bootstrap CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- /#wrapper -->

</head>
<body>
    
    <!-- Cabeçalho -->
    <?php require_once('../includes/header.php'); ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid"> 
            <div class="row bg-title">
                <div class="col-lg-12"> 
                    <h4 class="page-title">Todos os cadastros</h4>
                    <ol class="breadcrumb">
                        <li><a href="index_admin.php">Início</a></li>
                        <li class="active">Cadastros</li>
                    </ol>
                </div> 
                <!-- /.col-lg-12 -->
            </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">                               
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th><a href="create_cadastro.php">PRODUTOS / INSUMOS</a></th>
                                                <th><a href="#">CLIENTES</a></th>
                                                <th><a href="#">FUNCIONÁRIOS</a></th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td></td>
                                                <td></td>
                                                <td>Rhayander</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td></td>
                                                <td></td>
                                                <td>Bruna</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>                                        
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>

                            </div>
                            <?php require_once('../includes/footer.php'); ?>
                            <br>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            
            </div>
        
    </body>

    <!-- jQuery -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <!--Nice scroll JavaScript -->
        <script src="js/jquery.nicescroll.js"></script>
        <!--Morris JavaScript -->
        <script src="bower_components/raphael/raphael-min.js"></script>
        <script src="bower_components/morrisjs/morris.js"></script>
        <!--Efeitos no menu -->
        <script src="js/waves.js"></script>
        <!-- Tema padrão do admin -->
        <script src="js/myadmin.js"></script>
        <script src="js/dashboard1.js"></script>
        <script src="Projeto_ChatBot/Version1.1/app/controller/logoff.js"> </script>
    </html>