<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">

    <title> Todos os clientes </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Bootstrap CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery -->
    

</head>

<body>
    <!-- Preloader -->
    
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg "
                href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i
                class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="dashboard.html"><i
                    class="ti-user fa-fw"></i>&nbsp;<span class="hidden-xs"> Administração </span></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs hidden-lg
                         waves-effect waves-light"><i class="ti-arrow-circle-left ti-menu"></i>
                     </a></li>
                 </ul>
                 <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Pequisar..." class="form-control">
                            <a href=""><i class="ti-search"></i></a>
                        </form>
                    </li>
                    <li>
                        <a class="profile-pic" href="profile.html"> <img src="images/users/logo.jpeg" alt="user-img" width="36"
                            class="img-circle"><b class="hidden-xs">SIR Steakhouse</b> </a>
                        </li>
                    </ul>
                </div>

            </nav>
            <div class="navbar-default sidebar nicescroll" role="navigation">
                <div class="sidebar-nav navbar-collapse ">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="ti-search"></i> </button>
                                </span>
                            </div>
                        </li>
                        <li>
                            <a href="dashboard.html" class="waves-effect"><i class="glyphicon glyphicon-fire fa-fw"></i>
                            Dashboard</a>
                        </li>                      
                        <li>
                            <a href="C:/Desenvolvimento/Projeto_ChatBot/Version1.1/modulo_admin/list_clients.html" class="waves-effect"><i class="ti-layout fa-fw"></i>Clientes</a>
                        </li>
                        <li>
                            <a href="orders.html" class="waves-effect"><i class="ti-shopping-cart fa-fw"></i>Pedidos</a>
                        </li>

                        <li>
                            <a href="blank.html" class="waves-effect"><i class="ti-info fa-fw"></i>Informações</a>
                        </li>
                        <li>
                            <a href="C:/Desenvolvimento/Projeto_ChatBot/Version1.1/modulo_login/views/login.html" onclick="return logoff();" class="waves-effect"><i class="ti-power-off fa-fw"></i>Sair</a>
                        </li>
                    </ul>
                    <div class="center p-20">

                    </div>
                </div>
                <!-- /.sidebar-collapse -->
            </div>

            <!-- Conteúdo da listagem de clientes -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-12">
                            <h4 class="page-title">Todos os clientes</h4>
                            <ol class="breadcrumb">
                                <li><a href="dashboard.html">Dashboard</a></li>
                                <li class="active">Todos os clientes</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3>Basic Table</h3>
                                <p class="text-muted">Add class <code>.table</code></p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Role</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Deshmukh</td>
                                                <td>Prohaska</td>
                                                <td>@Genelia</td>
                                                <td>admin</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Deshmukh</td>
                                                <td>Gaylord</td>
                                                <td>@Ritesh</td>
                                                <td>member</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Sanghani</td>
                                                <td>Gusikowski</td>
                                                <td>@Govinda</td>
                                                <td>developer</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Roshan</td>
                                                <td>Rogahn</td>
                                                <td>@Hritik</td>
                                                <td>supporter</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Joshi</td>
                                                <td>Hickle</td>
                                                <td>@Maruti</td>
                                                <td>member</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Nigam</td>
                                                <td>Eichmann</td>
                                                <td>@Sonu</td>
                                                <td>supporter</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            
            </div>

            <script src="bower_components/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap JavaScript -->
            <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- Menu Plugin JavaScript -->
            <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
            <script src="js/jquery.nicescroll.js"></script>
            <script src="bower_components/raphael/raphael-min.js"></script>
            <script src="bower_components/morrisjs/morris.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!-- Tema padrão do admin -->
            <script src="js/myadmin.js"></script>
            <script src="js/dashboard1.js"></script>
            <script src="C:/Desenvolvimento/Projeto_ChatBot/Version1.1/modulo_login/controller/logoff.js"> </script>
            
        </body>
        </html>