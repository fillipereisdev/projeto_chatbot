<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">
    
    <title> Configuração de perfil </title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon.jpeg">
    <!-- Bootstrap CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- /#wrapper -->

</head>
<body style=" overflow-x: hidden; overflow-y: hidden;">
    
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="row bg-title">
                <div class="col-lg-12"> 
                    <h4 class="page-title">Configurações do perfil</h4>
                    <ol class="breadcrumb">
                        <li><a href="index_admin.php">Início</a></li>
                        <li class="active">Perfil</li>
                    </ol>
                </div> 
                <!-- /.col-lg-12 -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg"> <img width="100%" >
                            <div class="overlay-box">
                                <div class="user-content">
                                    <a href="javascript:void(0)"><img src="../images/users/logo.jpeg"
                                        class="thumb-lg img-circle" alt="img"></a>
                                        <h4 class="text-white">Rhayander Reis</h4>
                                        <h5 class="text-white">teste@gmail.com</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-purple"><i class="ti-facebook"></i></p>
                                    <h1>258</h1>
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue"><i class="ti-twitter"></i></p>
                                    <h1>125</h1>
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger"><i class="ti-dribbble"></i></p>
                                    <h1>556</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Nome completo:</label>
                                    <div class="col-md-12">
                                        <input type="text" required="text"
                                        class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email:</label>
                                        <div class="col-md-12">
                                            <input type="email" required="email"
                                            class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Senha:</label>
                                        <div class="col-md-12">
                                            <input type="password" required="password" class="form-control form-control-line">
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Salvar perfil</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php require_once('../includes/footer.php'); ?>
        <?php require_once('../includes/header.php'); ?>

    </div> 
</body>

<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<!--Morris JavaScript -->
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.js"></script>
<!--Efeitos no menu -->
<script src="js/waves.js"></script>
<!-- Tema padrão do admin -->
<script src="js/myadmin.js"></script>
<script src="js/dashboard1.js"></script>
<script src="Projeto_ChatBot/Version1.1/app/controller/logoff.js"> </script>
</html>