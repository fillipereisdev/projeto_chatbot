<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">

    <title> Painel administrativo </title>
    
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon.jpeg">
    <!-- Bootstrap CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    
</head>
<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg "
                href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i
                class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="../views/index_admin.php"><i
                    class="ti-user fa-fw"></i>&nbsp;<span class="hidden-xs"> Administração </span></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs hidden-lg
                         waves-effect waves-light"><i class="ti-arrow-circle-left ti-menu"></i>
                     </a></li>
                 </ul>
                 <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Pequisar..." class="form-control">
                            <a href="#"><i class="ti-search"></i></a>
                        </form>
                    </li>
                    <li>
                        <a class="profile-pic" href="profile.php"> <img src="../images/users/logo.jpeg" alt="user-img" width="36"
                            class="img-circle"><b class="hidden-xs">SIR Steakhouse</b> </a>
                        </li>
                    </ul>
                </div>

            </nav>
            <div class="navbar-default sidebar nicescroll" role="navigation">
                <div class="sidebar-nav navbar-collapse ">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="ti-search"></i> </button>
                                </span>
                            </div>
                        </li>
                        <li>
                            <a href="/Projeto_ChatBot/Version1.1/modulo_admin/views/dashboard.php" class="waves-effect"><i class="glyphicon glyphicon-fire fa-fw"></i>
                            Dashboard</a>
                        </li>

                        <li>
                            <a href="/Projeto_ChatBot/Version1.1/modulo_admin/views/cad_option.php" class="waves-effect"><i class="ti-layout fa-fw"></i>Cadastros</a>
                        </li>

                        <li>
                            <a href="/Projeto_ChatBot/Version1.1/modulo_admin/views/info.php" class="waves-effect"><i class="ti-info fa-fw"></i>Informações</a>
                        </li>
                        <li> 
                            <a href="/Projeto_ChatBot/Version1.1/modulo_login/views/login.php" onclick="return logoff();" class="waves-effect"><i class="ti-power-off fa-fw"></i>Sair</a>
                        </li>
                    </ul>
                    
                    <div class="center p-20">

                    </div>
                </div>
                <!-- /.sidebar-collapse -->
            </div>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-12">
                            <h4 class="page-title"></h4> 
                            <ol class="breadcrumb">

                            </ol>
                        </div>
                        <!-- Título do cabeçalho após logado no sistema -->
                        <h2>Bem vindo SIR Stakehouse!</h2>
                    </div>
                    
                </div>

            </div>

        </div>   

    </body>

    <!-- JS and jQuery -->

    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!--Nice scroll JavaScript -->
    <script src="../js/jquery.nicescroll.js"></script>
    <!--Morris JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.js"></script>
    <!--Efeitos no menu -->
    <script src="../js/waves.js"></script>
    <!-- Tema padrão do admin -->
    <script src="../js/myadmin.js"></script>
    <script src="../js/dashboard1.js"></script>
    <script src="/Projeto_ChatBot/Version1.1/app/controller/logoff.js"> </script>

    </html>